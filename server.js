const express = require('express')
const app = express()
const bodyParser = require("body-parser");

const port = 3000
app.use(bodyParser.json());

// Mongoose setup
const mongoose = require('mongoose');
const mongooseUrl = 'mongodb://localhost:27017/Hiring_App';
mongoose.Promise = Promise;
mongoose.createConnection(mongooseUrl, { server: { poolSize: 10 } });
const EmployeesSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  ssn: String,
  dob: String
});
const Employee = mongoose.model('Employee', EmployeesSchema);
mongoose.connect(mongooseUrl);

app.post('/Employees', async (req, res) => {
  let newEmployee = new Employee(req.body);
  try {
    const resp = await newEmployee.save();
    res.status(201).json(resp);
  } catch (err) {
    res.status(400).json(err);
  }
});

app.get('/Employees', async (req, res) => {
  try {
    const Employees = await Employee.find({});
    res.status(200).json(Employees);
  } catch (err) {
    res.status(400).json(err);
  }
});

app.get('/Employees/:employeeId', async (req, res) => {
  try {
    const Employees = await Employee.find({ _id: req.params.employeeId });
    if (Employees.length === 0) throw new Error('Employee not found');
    res.status(200).json(Employees[0]);
  } catch (err) {
    res.status(400).json(err);
  }
});

app.post('/Employees/:employeeId', async (req, res) => {
  try {
    const resp = await Employee.findByIdAndUpdate(req.params.employeeId,req.body,{ new: true });
    res.status(200).json(resp);
  } catch (err) {
    res.status(400).json(err);
  }
});

app.delete('/Employees/:employeeId', async (req, res) => {
  try {
    const resp = await Employee.findByIdAndRemove(req.params.employeeId);
    res.status(200).json(resp);
  } catch (err) {
    res.status(400).json(err);
  }
});

app.delete("/Employees", async (req, res) => {
  try {
    const resp = await Employee.deleteMany({});
    res.status(200).json(resp);
  } catch (err) {
    res.status(400).json(err);
  }
});

app.listen(port, () => {
  console.log(`Express server started on port ${port}`);
});