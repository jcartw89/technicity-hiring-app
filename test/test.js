const { expect } = require("chai");
const request = require("superagent");

const baseUrl = `localhost:3000`;

let employee1 = {
  firstName: "Jack",
  lastName: "Black",
  ssn: "0923430",
  dob: "12/3/95"
}

let employee2 = {
  firstName: "Mike",
  lastName: "Scott",
  ssn: "0927730",
  dob: "11/7/80"
}

let employee3 = {
  firstName: "Jim",
  lastName: "Bob",
  ssn: "0923499",
  dob: "10/1/77"
}

let employee1updated = {
  firstName: "Jack",
  lastName: "White",
  ssn: "0923430",
  dob: "12/3/95"
}

let employee1Id;
let employee2Id;
let employee3Id;

describe('Hiring app', () => {

  it('should clear database', () => {
    return request
      .delete(`${baseUrl}/Employees`)
      .send()
      .then(res => {
        expect(res.status).to.equal(200);
      });
  });

  it('should create an employee', () => {
    return request
      .post(`${baseUrl}/Employees`)
      .send(employee1)
      .then(res => {
        expect(res.status).to.equal(201);
        expect(res.body).to.have.property("_id");
        employee1Id = res.body._id;
        employee1._id = employee1Id;
        employee1updated._id = employee1Id;
      });
  });

  it('should create an employee', () => {
    return request
      .post(`${baseUrl}/Employees`)
      .send(employee2)
      .then(res => {
        expect(res.status).to.equal(201);
        expect(res.body).to.have.property("_id");
        employee2Id = res.body._id;
        employee2._id = employee2Id;
      });
  });

  it('should create an employee', () => {
    return request
      .post(`${baseUrl}/Employees`)
      .send(employee3)
      .then(res => {
        expect(res.status).to.equal(201);
        expect(res.body).to.have.property("_id");
        employee3Id = res.body._id;
        employee3._id = employee3Id;
      });
  });

  it('should update an employee\'s information', () => {
    return request
      .post(`${baseUrl}/Employees/${employee1Id}`)
      .send(employee1updated)
      .then(res => {
        expect(res.status).to.equal(200);
        Object.keys(employee1updated).forEach(key => expect(res.body[key]).to.equal(employee1updated[key]));
      });
  });

  it('should get an employee by id', () => {
    return request
      .get(`${baseUrl}/Employees/${employee2Id}`)
      .then(res => {
        expect(res.status).to.equal(200);
        expect(res.body.firstName).to.equal(employee2.firstName);
        expect(res.body.lastName).to.equal(employee2.lastName);
        expect(res.body.ssn).to.equal(employee2.ssn);
        expect(res.body.dob).to.equal(employee2.dob);
      });
  });

  it('should get all employees', () => {
    return request
      .get(`${baseUrl}/Employees`)
      .then(res => {
        expect(res.status).to.equal(200);
        expect(res.body.length).to.equal(3);
      });
  });

  it('should delete an employee by id', () => {
    return request
      .del(`${baseUrl}/Employees/${employee3Id}`)
      .send(employee3)
      .then(res => {
        expect(res.status).to.equal(200);
      });
  });

});

